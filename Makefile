# Give the project a name. The output binary will be given this name.
PROJECT = spi

# List the various paths in which the required header files may be found.
INCLUDE = .
INCLUDE += src/bridge
INCLUDE += src/common
INCLUDE += src/error

# List the various paths in which C or C++ file may be found.
VPATH = src/bridge:src/common:src/error

# List all of the C files that need to be compiled & linked.
CFILES = 

# List all of the C++ files that need to be compiled & linked.
CXXFILES = spi_example.cpp
CXXFILES += bridge.cpp
CXXFILES += criticalsectionlock.cpp
CXXFILES += stlink_device.cpp
CXXFILES += stlink_interface.cpp
CXXFILES += ErrLog.cpp

# Relative to the project directiory (repository), where should the object files and binaries be written.
OBJ_DIR ?= build/obj
BIN_DIR ?= build/bin

# Flags for compiling C files.
CFLAGS := -std=c99 -Wpedantic

# Flags for compiling C++ files.
CXXFLAGS := -std=c++98 -Wpedantic

# Preprocessor flags (apply to both C and C++ files).
CPPFLAGS := -Wall -g

# Libraries that need to be linked-in.
LIBS := lib/libSTLinkUSBDriver.so


# Use this prefix on command within rules to prevent the command from being echoed on the terminal.
QUIET	:= @

OBJS = $(CFILES:%.c=$(OBJ_DIR)/%.o)
OBJS += $(CXXFILES:%.cpp=$(OBJ_DIR)/%.o)

INCLUDES += $(patsubst %,-I%, . $(INCLUDE) )

# Tool paths.
PREFIX	?=
CC	= $(PREFIX)gcc
CXX	= $(PREFIX)g++
LD	= $(PREFIX)g++

all: $(BIN_DIR)/$(PROJECT).bin

$(OBJ_DIR)/%.o: %.c
	@printf "  CC\t$<\n"
	@mkdir -p $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) $(CPPFLAGS) $(INCLUDES) -o $@ -c $<

$(OBJ_DIR)/%.o: %.cpp
	@printf "  CXX\t$<\n"
	@mkdir -p $(dir $@)
	$(QUIET)$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(INCLUDES) -o $@ -c $<

$(BIN_DIR)/$(PROJECT).bin: $(OBJS)
	@printf "  LD\t$@\n"
	@mkdir -p $(dir $@)
	$(QUIET)$(LD) $^ $(LIBS) -o $@

clean:
	rm -rf build
